#include "red.h"

int amdTest(const std::string& loadedKernelModules, VkInstance instance, uint32_t deviceID) {
    bool amdgpuLoaded = (loadedKernelModules.find("amdgpu") != std::string::npos);
    bool radeonLoaded = (loadedKernelModules.find("radeon") != std::string::npos);

    if (amdgpuLoaded || radeonLoaded) {
        outputLog("AMD GPU:");
        if (amdgpuLoaded) {
            outputLog("Kernel module: AMDgpu");
        } else if (radeonLoaded) {
            outputLog("Kernel module: radeon");
        }
        if(instance == VK_NULL_HANDLE){
            outputLog("GPU Vulkan API Version: INCOMPATIBLE");
        } else {
            vkTest(instance, deviceID);
        }
        outputResult();
    }
    return 0;
}