#ifndef AMD_H
#define AMD_H

#include <iostream>
#include "../utils/vk.h"

int amdTest(const std::string& loadedKernelModules, VkInstance instance, uint32_t deviceID);

#endif // AMD_H
