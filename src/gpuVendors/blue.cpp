#include "blue.h"

int intelTest(const std::string& loadedKernelModules, VkInstance instance, uint32_t deviceID) {
    bool isIntelGPU = (loadedKernelModules.find("i915") != std::string::npos);

    if (isIntelGPU) {
        outputLog("Intel GPU:\nKernel module: i915");
        if(instance == VK_NULL_HANDLE){
            outputLog("GPU Vulkan API Version: INCOMPATIBLE");
        } else {
            vkTest(instance, deviceID);
        }
        outputResult();
    }
    return 0;
}