#ifndef NVIDIA_H
#define NVIDIA_H

#include <iostream>
#include "../utils/vk.h"

int nvidiaTest(const std::string& loadedKernelModules, VkInstance instance, uint32_t deviceID);

#endif // NVIDIA_H
