#ifndef INTEL_H
#define INTEL_H

#include <iostream>
#include "../utils/vk.h"

int intelTest(const std::string& loadedKernelModules, VkInstance instance, uint32_t deviceID);

#endif // INTEL_H
