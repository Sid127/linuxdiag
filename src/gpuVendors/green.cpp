#include "green.h"

int nvidiaTest(const std::string& loadedKernelModules, VkInstance instance, uint32_t deviceID) {
    bool nouveauLoaded = (loadedKernelModules.find("nouveau") != std::string::npos);
    bool nvidiaLoaded = (loadedKernelModules.find("nvidia") != std::string::npos);

    if (nouveauLoaded || nvidiaLoaded) {
        outputLog("NVIDIA GPU:");
        if (nouveauLoaded) {
            outputLog("Kernel module: nouveau");
        } else if (nvidiaLoaded) {
            outputLog("Kernel module: nvidia");
        }
        if(instance == VK_NULL_HANDLE){
            outputLog("GPU Vulkan API Version: INCOMPATIBLE");
        } else {
            vkTest(instance, deviceID);
        }
    }
    
    outputResult();
    return 0;
}