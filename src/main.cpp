#include "gpuVendors/red.h"
#include "gpuVendors/green.h"
#include "gpuVendors/blue.h"

#include <dirent.h>
#include <regex>

#ifdef TARGET_ARCH_64BIT
#include <QApplication>
#include <QMessageBox>
#include <QDir>
#include <QFile>
#endif

std::ofstream outputFile("/tmp/linuxdiag.log", std::ios::trunc);
int dxvk, vkd3d, vkd3dProton, proton; // see main.h for an explanation

struct GPUInfo {
    uint32_t vendorId;
    uint32_t deviceId;
};

std::vector<GPUInfo> getInstalledGPUs() {
    std::vector<GPUInfo> gpuList;
    DIR* dir;
    struct dirent* ent;

    if ((dir = opendir("/sys/class/drm")) != nullptr) {
        // /sys/class/drm/renderD* regex
        std::regex drmRenderRegex("renderD[0-9]+");

        while ((ent = readdir(dir)) != nullptr) {
            std::string dirName = ent->d_name;

            if (std::regex_match(dirName, drmRenderRegex)) {
                std::string fullPath = "/sys/class/drm/" + dirName + "/device/vendor";
                std::ifstream vendorFile(fullPath);
                if (!vendorFile) {
                    continue;
                }

                uint32_t vendorId;
                vendorFile >> std::hex >> vendorId;
                vendorFile.close();

                fullPath = "/sys/class/drm/" + dirName + "/device/device";
                std::ifstream deviceFile(fullPath);
                if (!deviceFile) {
                    continue;
                }

                uint32_t deviceId;
                deviceFile >> std::hex >> deviceId;
                deviceFile.close();

                gpuList.push_back({vendorId, deviceId});
            }
        }
        closedir(dir);
    }

    return gpuList;
}

int main(int argc, char *argv[]) {
    bool redirectOutput = false;

    for (int i = 1; i < argc; ++i) {
        if (std::string(argv[i]) == "--gui" || std::string(argv[i]) == "-g") {
            redirectOutput = true;
            break;
        }
        if (std::string(argv[i]) == "--help" || std::string(argv[i]) == "-h") {
            printf("Linuxdiag, a system diagnostics tool\n\n--gui  -g    Puts output in a dialog box\n--help -h   Displays this help message\n");
            return 0;
        }
    }

#ifdef TARGET_ARCH_64BIT
    if (redirectOutput) {
        std::cout.setstate(std::ios_base::failbit);
    }
#endif

    std::string kmods = readTextFile("/proc/bus/pci/devices");
    std::vector<GPUInfo> installedGPUs = getInstalledGPUs();

    if (installedGPUs.empty()) {
        outputLog("No GPUs found!");
    } else {
        VkInstance instance = vkInit();
        for (size_t i = 0; i < installedGPUs.size(); ++i) {
            if(installedGPUs[i].vendorId == 0x1002)
                amdTest(kmods, instance, installedGPUs[i].deviceId);
            else if (installedGPUs[i].vendorId == 0x8086)
                intelTest(kmods, instance, installedGPUs[i].deviceId);
            else if (installedGPUs[i].vendorId == 0x10DE)
                nvidiaTest(kmods, instance, installedGPUs[i].deviceId);
            else
                outputLog("Your GPU vendor doesn't seem to be reported. Please report this to the maintainer.");
        }

        if(instance != VK_NULL_HANDLE)
            vkKill(instance);
    }

    outputFile.close();

#ifdef TARGET_ARCH_64BIT
    if(redirectOutput){
        QApplication app(argc, argv);
        // Display the log in a message box
        std::ifstream logFile("/tmp/linuxdiag.log");
        std::string logContent((std::istreambuf_iterator<char>(logFile)), std::istreambuf_iterator<char>());
        logFile.close();

        QMessageBox msgBox;
        msgBox.setWindowTitle("Linuxdiag");
        msgBox.setText(QString::fromStdString(logContent));
        msgBox.exec();
    }
#endif

    return 0;
}
