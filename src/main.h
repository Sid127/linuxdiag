#ifndef MAIN_H
#define MAIN_H

extern std::ofstream outputFile;

/* It was getting too tiring having to type out results every time
a test was run, which is why these integers exist. Their values will
always range between 0-9, with each value indicating varying levels
of compatibility, 0 being the least, and 9 being the highest.
This system will also make it easier to give recommendations to users.

Note: not all ints here will go up to 9, and it depends on various conditions
required for usage. The 0-9 systems exists to future proof this, though it can
be extended if required */
extern int dxvk, vkd3d, vkd3dProton, proton;

#endif // MAIN_H