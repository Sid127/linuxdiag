#include "vk.h"

VkInstance vkInit(){
    volkInitialize();

    // Create Vulkan instance
    VkInstanceCreateInfo instanceCreateInfo = {};
    instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    
    const char* extensions[] = {
        VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME
    };
    instanceCreateInfo.enabledExtensionCount = 1;
    instanceCreateInfo.ppEnabledExtensionNames = extensions;

    VkInstance instance;
    VkResult result = vkCreateInstance(&instanceCreateInfo, nullptr, &instance);
    if (result != VK_SUCCESS) {
        return VK_NULL_HANDLE;
    }

    volkLoadInstance(instance);
    return instance;
}

// Function to clean up the Vulkan instance
void vkKill(VkInstance instance) {
    vkDestroyInstance(instance, nullptr);
}

VkPhysicalDevice SelectPhysicalDevice(VkInstance instance, uint32_t desiredDeviceID) {
    uint32_t deviceCount = 0;
    vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);
    if (deviceCount == 0) {
        return VK_NULL_HANDLE;
    }

    std::vector<VkPhysicalDevice> physicalDevices(deviceCount);
    vkEnumeratePhysicalDevices(instance, &deviceCount, physicalDevices.data());

    // Find the desired GPU based on the specified device ID
    for (const auto& physicalDevice : physicalDevices) {
        VkPhysicalDeviceProperties properties;
        vkGetPhysicalDeviceProperties(physicalDevice, &properties);
        if (properties.deviceID == desiredDeviceID) {
            return physicalDevice;
        }
    }

    return VK_NULL_HANDLE;
}

void vkTest(VkInstance instance, uint32_t desiredDeviceID){
    // Select the GPU based on the desired device ID
    VkPhysicalDevice selectedPhysicalDevice = SelectPhysicalDevice(instance, desiredDeviceID);
    if (selectedPhysicalDevice == VK_NULL_HANDLE) {
        return;
    }

    // Query GPU's Vulkan properties
    VkPhysicalDeviceProperties2 deviceProps{};
    deviceProps.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;

    VkPhysicalDeviceDriverProperties driverProps{};
    driverProps.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DRIVER_PROPERTIES;
    driverProps.pNext = nullptr;

    deviceProps.pNext = &driverProps;

    vkGetPhysicalDeviceProperties2(selectedPhysicalDevice, &deviceProps);

    uint32_t apiVersion = deviceProps.properties.apiVersion;
    uint32_t major = VK_VERSION_MAJOR(apiVersion);
    uint32_t minor = VK_VERSION_MINOR(apiVersion);
    uint32_t patch = VK_VERSION_PATCH(apiVersion);

    std::ostringstream stream;
    stream << "Selected GPU: " << deviceProps.properties.deviceName << std::endl;
    stream << "GPU Vulkan API Version: " << major << "." << minor << "." << patch << std::endl;
    stream << "GPU Vulkan Driver Name: " << driverProps.driverName;

    // TO-DO: check lib32 vulkan stuff
    // TO-DO: check for specific extension requirements
    if(major >= 1 && minor >= 3) {
        dxvk = 2;
        vkd3d = 1;
        if(deviceProps.properties.vendorID == 0x8086 || (deviceProps.properties.vendorID == 0x1002 && driverProps.driverName != "radv"))
            vkd3dProton = 1;
        else
            vkd3dProton = 3;
        proton = 2;
    } else if (major >= 1 && minor >= 1){
            dxvk = 1;
            vkd3d = 1;
            vkd3dProton = 2;
            proton = 1;
    } else {
        dxvk = 0;
        vkd3d = 0;
        vkd3dProton = 0;
        proton = 0;
    }

    std::string loglines = stream.str();
    outputLog(loglines);
}