#ifndef UTILS_H
#define UTILS_H

#include <cstdint>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

#include "../main.h"

std::string readTextFile(const std::string& filename);
void outputLog(const std::string& logline);
void outputResult();

#endif // UTILS_H
