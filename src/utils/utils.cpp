#include "utils.h"

std::string readTextFile(const std::string& filename) {
    std::ifstream inputFile(filename);

    if (!inputFile.is_open()) {
        throw std::runtime_error("Error opening file: " + filename);
    }

    std::string fileContent;
    inputFile >> std::noskipws; // Prevents whitespace skipping
    std::copy(
        std::istreambuf_iterator<char>(inputFile),
        std::istreambuf_iterator<char>(),
        std::back_inserter(fileContent)
    );

    inputFile.close();

    return fileContent;
}

void outputLog(const std::string& logline) {
    // Write to stdout (console)
    std::cout << logline << std::endl;

    // Write to file
    if (outputFile.is_open()) {
        outputFile << logline << std::endl;
    } else {
        std::cerr << "Error opening the file!" << std::endl;
    }
}

void outputResult(){
    std::ostringstream stream;

    switch (dxvk) {
        case 0:
            stream << "DXVK (DirectX 9/10/11): Unsupported" << std::endl;
            break;
        case 1:
            stream << "DXVK (DirectX 9/10/11): Up to v1.10.3" << std::endl;
            break;
        case 2:
            stream << "DXVK (DirectX 9/10/11): v2.0+ Supported" << std::endl;
            break;
        default:
            stream << "DXVK (DirectX 9/10/11): FROGGED UP (Report to maintainer)" << std::endl;
    }

    switch (vkd3d) {
        case 0:
            stream << "Wine-VKD3D (DirectX 12): Unsupported" << std::endl;
            break;
        case 1:
            stream << "Wine-VKD3D (DirectX 12): Supported" << std::endl;
            break;
        default:
            stream << "Wine-VKD3D (DirectX 12): FROGGED UP (Report to maintainer)" << std::endl;
    }

    switch (vkd3dProton) {
        case 0:
            stream << "VKD3D-Proton/Lutris (DirectX 12): Unsupported" << std::endl;
            break;
        case 1:
            stream << "VKD3D-Proton/Lutris (DirectX 12): Untested" << std::endl;
            break;
        case 2:
            stream << "VKD3D-Proton/Lutris (DirectX 12): Up to v2.8" << std::endl;
            break;
        case 3:
            stream << "VKD3D-Proton/Lutris (DirectX 12): v2.9+ Supported" << std::endl;
            break;
        default:
            stream << "VKD3D-Proton/Lutris (DirectX 12): FROGGED UP (Report to maintainer)" << std::endl;
    }

    switch (proton) {
        case 0:
            stream << "Proton: Unsupported" << std::endl;
            break;
        case 1:
            stream << "Proton: Up to v7.0-6" << std::endl;
            break;
        case 2:
            stream << "Proton: v8.0+ Supported" << std::endl;
            break;
        default:
            stream << "Proton: FROGGED UP (Report to maintainer)" << std::endl;
    }

    std::string loglines = stream.str();
    outputLog(loglines);
}