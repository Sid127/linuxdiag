#ifndef VK_NO_PROTOTYPES
#define VK_NO_PROTOTYPES

#include "../include/volk.h"
#include "utils.h"

#include <vector>

VkInstance vkInit();
void vkKill(VkInstance instance);
void vkTest(VkInstance instance, uint32_t desiredDeviceID);

#endif // VK_NO_PROTOTYPES