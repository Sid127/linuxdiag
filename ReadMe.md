# Linuxdiag - System Diagnosis Tool for Linux

Linuxdiag is a system diagnosis tool designed for Linux users, particularly for gaming purposes. This tool assists in checking whether all the necessary dependencies required for a smooth gaming experience are properly installed and loaded on the system.

### To-do
- [x] Identify running GPUs
- [x] Vulkan check (Partial, see TO-DO in code)
- [x] DXVK/VKD3D/Proton compatibility check (Too simple at the moment, see TO-DO in code)
- [] GLX, EGL check
- [] lava/llvmpipe fallback detection
- [] FSync/ESync detection in system wine
- [] Kernel config checks
- [] Connectivity to steam servers
- [] Sound hardware/software functionality

*Maybe more...*

### Building
```
mkdir build
cd build
cmake ..
make
```
